---
layout: markdown_page
title: "Lead Generation"
---
# Welcome to the Lead Generation Handbook  

The Lead Generation organization includes Content, Field Marketing, and Marketing Ops.   

## On this page
* [Lead Generation Handbooks](#handbooks)
* [References](#references)

## Lead Generation Handbooks:  <a name="handbooks"></a>

- [Content Marketing](/handbook/marketing/lead-generation/content)  
- [Field Marketing](/handbook/marketing/lead-generation/field-marketing/)
- [Online Marketing](/handbook/marketing/lead-generation/online-marketing/)  
- [Marketing Operations](/handbook/marketing/lead-generation/marketing-operations/)  

## References<a name="references"></a>

- [Q1 ‘16 Lead Generation.pptx](https://docs.google.com/presentation/d/1ePns2ln0bLb_SPodXkYC13HEWRoVrJzsOrHQ3aGWio0/edit#slide=id.p5)
- [Point webhook from mailchimp to marketo issue](https://gitlab.com/gitlab-com/www-gitlab-com/issues/427)
- [Replace wufoo forms with Marketo forms](https://gitlab.com/gitlab-com/www-gitlab-com/issues/422)
- [Marketo webhooks docs](http://developers.marketo.com/documentation/webhooks/)
- [Recurly data into Marketo](https://gitlab.com/gitlab-com/www-gitlab-com/issues/526)
- [Reseller Handbook](https://about.gitlab.com/handbook/resellers/)


